// Questão 2 //

//Criando uma classe de Veiculos contendo os atributos modelo, marca, ano//
//valorLocacao e quantidade de dias//
class Veiculos {

    //Criando atributos privados que só podem ser acessados pela propria classe//
    constructor(
        private _modelo: string,
        private _marca: string,
        private _ano: number,
        private _valorLocacao: number,
        private _quantidadeDias: number
    ){}

//Criando os getters//
    get modelo(){
        return this._modelo 
    }

    get marca(){
        return this._marca 
    }

    get ano(){
        return this._ano 
    }

    get valorLocacao(){
        return this._valorLocacao 
    }

    get quantidadeDias(){
        return this._quantidadeDias 
    }


//Criando os setters//

//Quando a definição de modelo estiver em branco retorna uma mensagem inválida.//
    set modelo(modelo: string){
        if(modelo == '') {
            throw new Error("Modelo inaceitavel")
        }
        this._modelo = modelo
    }

//Quando a definição de marca estiver em branco retorna uma mensagem inválida.//
    set marca(marca: string){
        if(marca == '') {
            throw new Error("Marca inaceitavel")
        }
        this._marca = marca
    }

//Quando a definição de ano for menor ou igual a zero retorna uma mensagem inválida.//
    set ano(ano: number){
        if(ano <= 0) {
            throw new Error("Ano inaceitavel")
        }
        this._ano = ano
    }

//Quando a definição de valorLocacao for menor ou igual a zero retorna uma mensagem inválida.//
    set valorLocacao(valorLocacao: number){
        if(valorLocacao <= 0) {
            throw new Error("Valor da Locacao inaceitavel")
        }
        this._valorLocacao = valorLocacao
    }

//Quando a definição de quantidadeDias for menor ou igual a zero retorna uma mensagem inválida.//
    set quantidadeDias(quantidadeDias: number){
        if(quantidadeDias <= 0) {
            throw new Error("Quantidade de Dias inaceitavel")
        }
        this._quantidadeDias = quantidadeDias
    }

//Metodo que recebe a quantidade de dias e valor da locação. //
//Ele retorna o resultado do calculo da multiplicação da quantidade de dias pelo valor da locação //
    passeio(){
        let valor = this.quantidadeDias * this.valorLocacao
        return valor
    }
}

//Criando um objeto carro a partir do modelo Veiculos//
const carro = new Veiculos('Meriva', 'Chevrolet', 2012, 70, 5)

//Mostrando as informações do carro//
console.log(carro)
console.log(`Quantidade de dias: ${carro.quantidadeDias}\nValor da locação: ${carro.valorLocacao}\nResultado: ${carro.passeio()}\n\n`);

//Exemplos//

// try{
//     carro.marca = ''
// } catch (err:any){
//     console.log(err.message)
// }

// try{
//     carro.modelo = ''
// } catch (err:any){
//     console.log(err.message)
// }

// try{
//     carro.ano = 0
// } catch (err:any){
//     console.log(err.message)
// }

// try{
//     carro.quantidadeDias = 0
//     carro.valorLocacao = 0
// } catch (err:any){
//     console.log(err.message);
// }

//console.log(carro)
//console.log(`Quantidade de dias: ${carro.quantidadeDias}\nValor da locação: ${carro.valorLocacao}\nResultado: ${carro.passeio()}`);