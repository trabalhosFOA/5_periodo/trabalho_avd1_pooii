import { Request, Response } from 'express'
import { createUser } from './services/CreateUser'

export function helloWorld(request: Request, response: Response){

    return response.json({ message: 'Hello World'})
}