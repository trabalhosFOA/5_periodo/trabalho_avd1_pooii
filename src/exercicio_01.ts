// Questão 1 //

//Criando uma classe Pessoa com os atributos nome, sexo e idade.//
class Pessoa {
    constructor(
        private _nome: string,
        private _sexo: string,
        private _idade: number
    ){}

//Criando os getters//
    get nome(){
        return this._nome
    }

    get sexo(){
        return this._sexo
    }

    get idade(){
        return this._idade
    }

//Criando os setters//

//Quando o nome for em branco retorna uma mensagem de nome inválido.//
    set nome(nome: string){
        if(nome == ''){
            throw new Error(
                "Nome Inválido"
                )
        }
        this._nome = nome
    }

//Quando a definição de sexo for diferente de Masculino ou Feminino retorna uma mensagem inválida.//
    set sexo(sexo: string){
        if(sexo == "Masculino" || sexo == "Feminino"){
            this._sexo = sexo
        } else {
            throw new Error(
                "Erro! Sexo tem que ser definido como Masculino ou Feminino."
                )
        }
    }
//Quando a definição de idade for 0 retorna uma mensagem de idade inválida.//
    set idade(idade: number){
        if(idade <= 0){
            throw new Error(
                "Erro! Idade não pode ser definida como menor ou igual a zero."
                )
        }
        this._idade = idade
    }
    //Criando um método que recebe a idade//
    //Quando a idade for maior ou igual a 18 exibe uma mensagem que a pessoa é maior de idade//
    //Caso contrário exibe uma mensagem que a pessoa é menor de idade.
    public receberIdade(idade: number){
        if(idade >= 18){
            console.log(
                "Pessoa é maior de idade."
                )
                //this._idade = idade
        } else {
            console.log(
                "Pessoa é menor de idade."
                )
                //this._idade = idade
        }
    }
}

//Criando um objeto p1 a partir do modelo Pessoa//
const p1 = new Pessoa('Déborah', 'Feminino', 26);
console.log(p1);


//Alteração do atributo nome//
try {
    p1.nome = ''
    console.log(p1.nome)
} catch (err:any) {
    console.log(err.message)
}

//Alteração do atributo sexo//
try {
    p1.sexo = ""
    console.log(p1.sexo)
} catch (err:any) {
    console.log(err.message)
}

//Alteração do atributo idade//
try {
    p1.idade = 15
    console.log(p1.idade)
} catch (err:any) {
    console.log(err.message)
}


//Chamando o metodo receberIdade e alterando a idade//
p1.receberIdade(15)

//Mostrando a pessoa com os atributos alterados//
console.log(p1);
