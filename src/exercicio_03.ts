// Questão 3 //

//Criando uma classe Fatura com os atributos números, descrição, quantidadeComprada e preço//
class Fatura {
    constructor(
        private _numeros: number,
        private _descricao: string,
        private _quantidadeComprada: number,
        private _preco: number,
    ) {}

    //Crie um método valor da Fatura para multiplicar a quantidade comprada pelo preço.//
    valorDaFatura() {
        let valor = this._quantidadeComprada * this._preco
        return valor
    }
}

//Criando um objeto computador a partir do modelo Fatura//
const computador = new Fatura(1, 'Computador', 1, 4500)

//Mostrando as informações do objeto criado//
console.log(computador)

//Mostrando o valor da fatura//
console.log("Valor total da Fatura:", computador.valorDaFatura())