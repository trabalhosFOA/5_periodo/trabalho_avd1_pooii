// Questão 4 //

//Criando uma classe pai para calcular o imposto de renda com os atributos nome e renda anual.//
class Imposto {
    constructor (
        private _nome: string,
        private _rendaAnual: number
        ){}

//Criando os getters//
        get nome(){
            return this._nome;
        }

        get rendaAnual(){
            return this._rendaAnual;
        }

//Criando os setters//

//Quando o nome estiver em branco retorna uma mensagem invalida//
        set nome(nome: string){
            if(nome == ''){
                throw new Error("Nome invalido")
            }
            this._nome = nome
        }

//Quando a renda anual for menor ou igual a zero retorna uma mensagem invalida//
        set rendaAnual(rendaAnual: number){
            if(rendaAnual <= 0){
                throw new Error("Renda Invalida")
            }
            this._rendaAnual = rendaAnual
        }
}


//Criando uma classe filha chamada Pessoa Física que herda da classe Imposto contem o atributo gastos com saúde//
class PFisica extends Imposto{
    private _gastosSaude: number
    constructor(_nome: string, _rendaAnual: number, gastosSaude: number )
    {
        super(_nome, _rendaAnual)
        this._gastosSaude = gastosSaude
    }

//Criando o get//
    get gastosSaude(){
        return this._gastosSaude;
    }

//Criando o set//
    set gastosSaude(gastosSaude: number){
        if(gastosSaude <= 0){
            throw new Error("Gasto Invalido")
        }
        this._gastosSaude = gastosSaude
    }


// Pessoa Física: Pessoas cuja renda foi abaixo de 20.000,00 pagam 15% de imposto. //
// Pessoas com renda maior ou igual a 20.000,00 pagam imposto de 25%. //
// Se apessoa teve gastos com saúde, 50% destes gastos são abatidos no imposto. ///
    calculoImposto(){
        var imposto
        if(this.rendaAnual < 2000){
            imposto = 0.15 * this.rendaAnual;
        }else{
            imposto = 0.25 * this.rendaAnual;
        }
        // var imposto = this.rendaAnual*(
        //     this.rendaAnual >= 20000 ? 0.25 : 0.15
        //     )
        var saude = this.gastosSaude*(
            this.gastosSaude != 0 ? 0.5 : 0
            )
        return imposto - saude
    }
}


//Criando uma classe filha chamada Pessoa Jurídica que herda da classe Imposto contem o atributo número de funcionarios//
class pessoaJuridica extends Imposto{
    private _numeroFuncionarios: number
    constructor(_nome: string, _rendaAnual: number, numeroFuncionarios: number )
    {
        super(_nome, _rendaAnual)
        this._numeroFuncionarios = numeroFuncionarios
    }

//Criando o get//
    get numeroFuncionarios(){
        return this._numeroFuncionarios;
    }

//Criando o set//
    set numeroFuncionarios(numeroFuncionarios: number){
        if(numeroFuncionarios <= 0){
            throw new Error("Quantidade Invalida")
        }
        this._numeroFuncionarios = numeroFuncionarios
    }

///Regras para Calculo de Imposto///
// Pessoa Jurídica: Pessoas jurídicas pagam 16% de imposto, porém, se a empresa
// possuir mais de 10 funcionários, ela paga 14% de imposto. Exemplo: Uma empresa
// Trabalho de POO II 2
// cuja renda foi de 400.000,00 e possui 25 funcionários, o imposto fica: 400.000,00 *
// 14% = 56.000,00
    calculoImposto(): number {  
        if(this.numeroFuncionarios < 10){
            return this.rendaAnual * 0.16
        } else {
            return this.rendaAnual * 0.14
        }
      }
}

const pFisica = new PFisica('Déborah', 50000, 2000)

// pFisica.gastosSaude = 700

console.log('Imposto:', pFisica.calculoImposto())
console.log(pFisica)



const pJuridica = new pessoaJuridica('DuosPlay Games', 400000, 25)
console.log('Imposto:', pJuridica.calculoImposto())
console.log(pJuridica)
