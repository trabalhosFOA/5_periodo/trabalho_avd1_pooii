//Questão 5//

//Criando uma classe chamada Vendedores com os atributos nome, salario e valor da venda.//
class Vendedores {
    constructor(
        private _nome: string,
        private _salario: number,
        private _valorVenda: number
    ){}

//Criando os getters//
    get nome() {
        return this._nome
    }

    get salario() {
        return this._salario
    }

    get valorVenda(){
        return this._valorVenda
    }

//Criando os setters//
    set nome(nome: string){
        if(nome == ''){
            throw new Error("Nome invalido")
        }
        this._nome = nome
    }

    set salario(salario: number){
        if(salario <= 0){
            throw new Error("Salario invalido")
        }
        this._salario = salario
    }

    set valorVenda(valorVenda: number){
        if(valorVenda <= 0){
            throw new Error("Valor da Venda invalido")
        }
        this._valorVenda = valorVenda
    }

//Criando um metodo que retorna o resultado do calculo de desconto do salario//
    descontoSalario(){
        return this._salario * 0.08
    }
}

// Criando uma classe filha chamada Pessoa Física que herda de Vendedores com o atributo região,
// crie o getter e setter com validação para aceitar somente as regiões sul, sudeste,
// centro-oeste, norte e nordeste.//

class pessoaFisica extends Vendedores {
    static REGIOES = ['sul', 'sudeste', 'centrooeste', 'norte', 'nordeste']
    private _regiao: string;

    constructor(_nome: string, _salario: number, _valorVenda: number, regiao: string)
    {
        super(_nome, _salario, _valorVenda);
        this._regiao = regiao;
    }

    get regiao() {
        return this._regiao
    }
    
    set regiao(regiao: string){
        if(!pessoaFisica.REGIOES.includes(regiao)){
            throw new Error("Regiao Invalida")
        }
        this._regiao = regiao
    }

// Criando um método para calcular a comissão.// 
//Se a região for sul a comissão será de 10% sobre o valor da venda. //
//Se a região for sudeste a comissão será de 12% sobre o valor da venda, //
//Se a região for centrooeste a comissão será de 14% sobre o valor da venda, //
//Se a região for norte a comissão será de 15% sobre o valor da venda, //
//Se a região for nordeste a comissão será de 17% sobre o valor da venda. //

    comissao(){
        if(this.regiao == 'sul'){
            return this.valorVenda * 0.10
        } else if (this.regiao == 'sudeste'){
            return this.valorVenda * 0.12
        } else if (this.regiao == 'centrooste'){
            return this.valorVenda * 0.14
        } else if (this.regiao == 'norte'){
            return this.valorVenda * 0.15
        } else {
            return this.valorVenda * 0.17
        }
    }

//Criando o método para calcular o salário total que será o salario mais a comissão.
    salarioTotal(){
        return this.salario + this.comissao()
    }
}


// Criando uma classe filha chamada Pessoa Jurídica que herda de Vendedores com os atributos nome da empresa, total de funcionários. //
class PessoaJuridica extends Vendedores {
    private _nomeEmpresa: string;
    private _totalFuncionarios: number;

    constructor(_nome: string, _salario: number, _valorVenda: number, nomeEmpresa: string, totalFuncionarios: number)
    {
        super(_nome, _salario, _valorVenda);
        this._nomeEmpresa = nomeEmpresa;
        this._totalFuncionarios = totalFuncionarios;
    }

//Criando os getters//    
    get nomeEmpresa(){
        return this._nomeEmpresa
    }

    get totalFuncionarios(){
        return this._totalFuncionarios
    }

//Criando os setters//
    set nomeEmpresa(nomeEmpresa: string){
        if(nomeEmpresa == ''){
            throw new Error("Nome da Empresa invalido")
        }
        this._nomeEmpresa = nomeEmpresa
    }

    set totalFuncionarios(totalFuncionarios: number){
        if(totalFuncionarios <= 0){
            throw new Error("Total de Funcionarios incorreto")
        }
        this._totalFuncionarios = totalFuncionarios
    }

// Criando um método para calcular a comissão. //
//Se o valor da venda for menor que 5.000,00 o valor da comissão será de 2% sobre o valor da venda.//
//Se o valor da venda for maior ou igual a 5.000,00 e menor que 10.000,00 o valor da comissão será de 4% sobre o valor da venda.// 
//Se o valor da venda for maior ou igual a 10.000,00 o valor da comissão será de 6% sobre o valor da venda. //    
    calComissao(){
        if(this.valorVenda < 5000){
            return this.valorVenda * 0.02
        } else if (this.valorVenda >= 5000 && this.valorVenda <= 10000){
            return this.valorVenda * 0.04
        } else {
            return this.valorVenda * 0.06
        }
    }

//Criando um método salario Total que será a soma do salario mais comissão e mais R$ 200,00 se o número de funcionários for menor que
//100 ou mais R$ 300,00 se o número de funcionários for maior ou igual a 100.
    totalSalario(){
        if(this.totalFuncionarios >= 100){
            return this.salario + this.calComissao() + 300 
        } else {
            return this.salario + this.calComissao() + 200
        }
    }
}

//Criando um objeto vendedor a partir do modelo Vendedores
const pF = new pessoaFisica('Déborah', 6500, 50000, 'sudeste')
const pJ = new PessoaJuridica('Luna', 3000, 100000, 'Petshop', 10)



console.log(pF)
console.log(pJ)

console.log("Desconto do salario de Pessoa Fisica:", pF.descontoSalario())
console.log("Desconto do salario de Pessoa Juridica:", pJ.descontoSalario())

try {
    pF.nome = 'Kovu'
    console.log(pF.nome)
} catch (err:any) {
    console.log(err.message)
}

// try {
//     pF.salario = 1000
//     console.log(pF.salario)
// } catch (err:any) {
//     console.log(err.message)
// }


console.log(pF.salarioTotal())
console.log(pJ.totalSalario())