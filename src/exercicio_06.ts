// Questão 6 //

//Criando uma interface chamada IEndereço com os seguintes atributos rua, numero, bairro e cidade.//
interface IEndereço {
    rua: string;
    numero: number;
    bairro: string;
    cidade: string;
}

//Variavel endereco do tipo da interface criada anteriormente.//
let endereco: IEndereço

//Atribuindo um endereço, uma rua, numero, bairro e cidade à variavel criada.//
endereco = {
    rua: 'Rua Lais Neto dos Reis',
    numero: 276,
    bairro: "Vila Julieta",
    cidade: "Resende"
}

//Utilize o console.log para exibir o conteúdo da variável endereço.//
console.log("Endereço Registrado:", endereco)
