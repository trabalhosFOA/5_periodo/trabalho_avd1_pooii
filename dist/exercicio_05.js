"use strict";
//Questão 5//
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//Criando uma classe chamada Vendedores com os atributos nome, salario e valor da venda.//
var Vendedores = /** @class */ (function () {
    function Vendedores(_nome, _salario, _valorVenda) {
        this._nome = _nome;
        this._salario = _salario;
        this._valorVenda = _valorVenda;
    }
    Object.defineProperty(Vendedores.prototype, "nome", {
        //Criando os getters//
        get: function () {
            return this._nome;
        },
        //Criando os setters//
        set: function (nome) {
            if (nome == '') {
                throw new Error("Nome invalido");
            }
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Vendedores.prototype, "salario", {
        get: function () {
            return this._salario;
        },
        set: function (salario) {
            if (salario <= 0) {
                throw new Error("Salario invalido");
            }
            this._salario = salario;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Vendedores.prototype, "valorVenda", {
        get: function () {
            return this._valorVenda;
        },
        set: function (valorVenda) {
            if (valorVenda <= 0) {
                throw new Error("Valor da Venda invalido");
            }
            this._valorVenda = valorVenda;
        },
        enumerable: false,
        configurable: true
    });
    //Criando um metodo que retorna o resultado do calculo de desconto do salario//
    Vendedores.prototype.descontoSalario = function () {
        return this._salario * 0.08;
    };
    return Vendedores;
}());
// Criando uma classe filha chamada Pessoa Física que herda de Vendedores com o atributo região,
// crie o getter e setter com validação para aceitar somente as regiões sul, sudeste,
// centro-oeste, norte e nordeste.//
var pessoaFisica = /** @class */ (function (_super) {
    __extends(pessoaFisica, _super);
    function pessoaFisica(_nome, _salario, _valorVenda, regiao) {
        var _this = _super.call(this, _nome, _salario, _valorVenda) || this;
        _this._regiao = regiao;
        return _this;
    }
    Object.defineProperty(pessoaFisica.prototype, "regiao", {
        get: function () {
            return this._regiao;
        },
        set: function (regiao) {
            if (!pessoaFisica.REGIOES.includes(regiao)) {
                throw new Error("Regiao Invalida");
            }
            this._regiao = regiao;
        },
        enumerable: false,
        configurable: true
    });
    // Criando um método para calcular a comissão.// 
    //Se a região for sul a comissão será de 10% sobre o valor da venda. //
    //Se a região for sudeste a comissão será de 12% sobre o valor da venda, //
    //Se a região for centrooeste a comissão será de 14% sobre o valor da venda, //
    //Se a região for norte a comissão será de 15% sobre o valor da venda, //
    //Se a região for nordeste a comissão será de 17% sobre o valor da venda. //
    pessoaFisica.prototype.comissao = function () {
        if (this.regiao == 'sul') {
            return this.valorVenda * 0.10;
        }
        else if (this.regiao == 'sudeste') {
            return this.valorVenda * 0.12;
        }
        else if (this.regiao == 'centrooste') {
            return this.valorVenda * 0.14;
        }
        else if (this.regiao == 'norte') {
            return this.valorVenda * 0.15;
        }
        else {
            return this.valorVenda * 0.17;
        }
    };
    //Criando o método para calcular o salário total que será o salario mais a comissão.
    pessoaFisica.prototype.salarioTotal = function () {
        return this.salario + this.comissao();
    };
    pessoaFisica.REGIOES = ['sul', 'sudeste', 'centrooeste', 'norte', 'nordeste'];
    return pessoaFisica;
}(Vendedores));
// Criando uma classe filha chamada Pessoa Jurídica que herda de Vendedores com os atributos nome da empresa, total de funcionários. //
var PessoaJuridica = /** @class */ (function (_super) {
    __extends(PessoaJuridica, _super);
    function PessoaJuridica(_nome, _salario, _valorVenda, nomeEmpresa, totalFuncionarios) {
        var _this = _super.call(this, _nome, _salario, _valorVenda) || this;
        _this._nomeEmpresa = nomeEmpresa;
        _this._totalFuncionarios = totalFuncionarios;
        return _this;
    }
    Object.defineProperty(PessoaJuridica.prototype, "nomeEmpresa", {
        //Criando os getters//    
        get: function () {
            return this._nomeEmpresa;
        },
        //Criando os setters//
        set: function (nomeEmpresa) {
            if (nomeEmpresa == '') {
                throw new Error("Nome da Empresa invalido");
            }
            this._nomeEmpresa = nomeEmpresa;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PessoaJuridica.prototype, "totalFuncionarios", {
        get: function () {
            return this._totalFuncionarios;
        },
        set: function (totalFuncionarios) {
            if (totalFuncionarios <= 0) {
                throw new Error("Total de Funcionarios incorreto");
            }
            this._totalFuncionarios = totalFuncionarios;
        },
        enumerable: false,
        configurable: true
    });
    // Criando um método para calcular a comissão. //
    //Se o valor da venda for menor que 5.000,00 o valor da comissão será de 2% sobre o valor da venda.//
    //Se o valor da venda for maior ou igual a 5.000,00 e menor que 10.000,00 o valor da comissão será de 4% sobre o valor da venda.// 
    //Se o valor da venda for maior ou igual a 10.000,00 o valor da comissão será de 6% sobre o valor da venda. //    
    PessoaJuridica.prototype.calComissao = function () {
        if (this.valorVenda < 5000) {
            return this.valorVenda * 0.02;
        }
        else if (this.valorVenda >= 5000 && this.valorVenda <= 10000) {
            return this.valorVenda * 0.04;
        }
        else {
            return this.valorVenda * 0.06;
        }
    };
    //Criando um método salario Total que será a soma do salario mais comissão e mais R$ 200,00 se o número de funcionários for menor que
    //100 ou mais R$ 300,00 se o número de funcionários for maior ou igual a 100.
    PessoaJuridica.prototype.totalSalario = function () {
        if (this.totalFuncionarios >= 100) {
            return this.salario + this.calComissao() + 300;
        }
        else {
            return this.salario + this.calComissao() + 200;
        }
    };
    return PessoaJuridica;
}(Vendedores));
//Criando um objeto vendedor a partir do modelo Vendedores
var pF = new pessoaFisica('Déborah', 6500, 50000, 'sudeste');
var pJ = new PessoaJuridica('Luna', 3000, 100000, 'Petshop', 10);
console.log(pF);
console.log(pJ);
console.log("Desconto do salario de Pessoa Fisica:", pF.descontoSalario());
console.log("Desconto do salario de Pessoa Juridica:", pJ.descontoSalario());
try {
    pF.nome = 'Kovu';
    console.log(pF.nome);
}
catch (err) {
    console.log(err.message);
}
// try {
//     pF.salario = 1000
//     console.log(pF.salario)
// } catch (err:any) {
//     console.log(err.message)
// }
console.log(pF.salarioTotal());
console.log(pJ.totalSalario());
