"use strict";
// Questão 1 //
//Criando uma classe Pessoa com os atributos nome, sexo e idade.//
var Pessoa = /** @class */ (function () {
    function Pessoa(_nome, _sexo, _idade) {
        this._nome = _nome;
        this._sexo = _sexo;
        this._idade = _idade;
    }
    Object.defineProperty(Pessoa.prototype, "nome", {
        //Criando os getters//
        get: function () {
            return this._nome;
        },
        //Criando os setters//
        //Quando o nome for em branco retorna uma mensagem de nome inválido.//
        set: function (nome) {
            if (nome == '') {
                throw new Error("Nome Inválido");
            }
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "sexo", {
        get: function () {
            return this._sexo;
        },
        //Quando a definição de sexo for diferente de Masculino ou Feminino retorna uma mensagem inválida.//
        set: function (sexo) {
            if (sexo == "Masculino" || sexo == "Feminino") {
                this._sexo = sexo;
            }
            else {
                throw new Error("Erro! Sexo tem que ser definido como Masculino ou Feminino.");
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "idade", {
        get: function () {
            return this._idade;
        },
        //Quando a definição de idade for 0 retorna uma mensagem de idade inválida.//
        set: function (idade) {
            if (idade <= 0) {
                throw new Error("Erro! Idade não pode ser definida como menor ou igual a zero.");
            }
            this._idade = idade;
        },
        enumerable: false,
        configurable: true
    });
    //Criando um método que recebe a idade//
    //Quando a idade for maior ou igual a 18 exibe uma mensagem que a pessoa é maior de idade//
    //Caso contrário exibe uma mensagem que a pessoa é menor de idade.
    Pessoa.prototype.receberIdade = function (idade) {
        if (idade >= 18) {
            console.log("Pessoa é maior de idade.");
            //this._idade = idade
        }
        else {
            console.log("Pessoa é menor de idade.");
            //this._idade = idade
        }
    };
    return Pessoa;
}());
//Criando um objeto p1 a partir do modelo Pessoa//
var p1 = new Pessoa('Déborah', 'Feminino', 26);
console.log(p1);
//Alteração do atributo nome//
try {
    p1.nome = '';
    console.log(p1.nome);
}
catch (err) {
    console.log(err.message);
}
//Alteração do atributo sexo//
try {
    p1.sexo = "";
    console.log(p1.sexo);
}
catch (err) {
    console.log(err.message);
}
//Alteração do atributo idade//
try {
    p1.idade = 15;
    console.log(p1.idade);
}
catch (err) {
    console.log(err.message);
}
//Chamando o metodo receberIdade e alterando a idade//
p1.receberIdade(15);
//Mostrando a pessoa com os atributos alterados//
console.log(p1);
