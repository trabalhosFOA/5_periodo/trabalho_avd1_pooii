"use strict";
// Questão 4 //
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//Criando uma classe pai para calcular o imposto de renda com os atributos nome e renda anual.//
var Imposto = /** @class */ (function () {
    function Imposto(_nome, _rendaAnual) {
        this._nome = _nome;
        this._rendaAnual = _rendaAnual;
    }
    Object.defineProperty(Imposto.prototype, "nome", {
        //Criando os getters//
        get: function () {
            return this._nome;
        },
        //Criando os setters//
        //Quando o nome estiver em branco retorna uma mensagem invalida//
        set: function (nome) {
            if (nome == '') {
                throw new Error("Nome invalido");
            }
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Imposto.prototype, "rendaAnual", {
        get: function () {
            return this._rendaAnual;
        },
        //Quando a renda anual for menor ou igual a zero retorna uma mensagem invalida//
        set: function (rendaAnual) {
            if (rendaAnual <= 0) {
                throw new Error("Renda Invalida");
            }
            this._rendaAnual = rendaAnual;
        },
        enumerable: false,
        configurable: true
    });
    return Imposto;
}());
//Criando uma classe filha chamada Pessoa Física que herda da classe Imposto contem o atributo gastos com saúde//
var PFisica = /** @class */ (function (_super) {
    __extends(PFisica, _super);
    function PFisica(_nome, _rendaAnual, gastosSaude) {
        var _this = _super.call(this, _nome, _rendaAnual) || this;
        _this._gastosSaude = gastosSaude;
        return _this;
    }
    Object.defineProperty(PFisica.prototype, "gastosSaude", {
        //Criando o get//
        get: function () {
            return this._gastosSaude;
        },
        //Criando o set//
        set: function (gastosSaude) {
            if (gastosSaude <= 0) {
                throw new Error("Gasto Invalido");
            }
            this._gastosSaude = gastosSaude;
        },
        enumerable: false,
        configurable: true
    });
    // Pessoa Física: Pessoas cuja renda foi abaixo de 20.000,00 pagam 15% de imposto. //
    // Pessoas com renda maior ou igual a 20.000,00 pagam imposto de 25%. //
    // Se apessoa teve gastos com saúde, 50% destes gastos são abatidos no imposto. ///
    PFisica.prototype.calculoImposto = function () {
        var imposto;
        if (this.rendaAnual < 2000) {
            imposto = 0.15 * this.rendaAnual;
        }
        else {
            imposto = 0.25 * this.rendaAnual;
        }
        // var imposto = this.rendaAnual*(
        //     this.rendaAnual >= 20000 ? 0.25 : 0.15
        //     )
        var saude = this.gastosSaude * (this.gastosSaude != 0 ? 0.5 : 0);
        return imposto - saude;
    };
    return PFisica;
}(Imposto));
//Criando uma classe filha chamada Pessoa Jurídica que herda da classe Imposto contem o atributo número de funcionarios//
var pessoaJuridica = /** @class */ (function (_super) {
    __extends(pessoaJuridica, _super);
    function pessoaJuridica(_nome, _rendaAnual, numeroFuncionarios) {
        var _this = _super.call(this, _nome, _rendaAnual) || this;
        _this._numeroFuncionarios = numeroFuncionarios;
        return _this;
    }
    Object.defineProperty(pessoaJuridica.prototype, "numeroFuncionarios", {
        //Criando o get//
        get: function () {
            return this._numeroFuncionarios;
        },
        //Criando o set//
        set: function (numeroFuncionarios) {
            if (numeroFuncionarios <= 0) {
                throw new Error("Quantidade Invalida");
            }
            this._numeroFuncionarios = numeroFuncionarios;
        },
        enumerable: false,
        configurable: true
    });
    ///Regras para Calculo de Imposto///
    // Pessoa Jurídica: Pessoas jurídicas pagam 16% de imposto, porém, se a empresa
    // possuir mais de 10 funcionários, ela paga 14% de imposto. Exemplo: Uma empresa
    // Trabalho de POO II 2
    // cuja renda foi de 400.000,00 e possui 25 funcionários, o imposto fica: 400.000,00 *
    // 14% = 56.000,00
    pessoaJuridica.prototype.calculoImposto = function () {
        if (this.numeroFuncionarios < 10) {
            return this.rendaAnual * 0.16;
        }
        else {
            return this.rendaAnual * 0.14;
        }
    };
    return pessoaJuridica;
}(Imposto));
var pFisica = new PFisica('Déborah', 50000, 2000);
// pFisica.gastosSaude = 700
console.log('Imposto:', pFisica.calculoImposto());
console.log(pFisica);
var pJuridica = new pessoaJuridica('DuosPlay Games', 400000, 25);
console.log('Imposto:', pJuridica.calculoImposto());
console.log(pJuridica);
