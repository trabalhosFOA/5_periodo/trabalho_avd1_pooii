"use strict";
// Questão 3 //
//Criando uma classe Fatura com os atributos números, descrição, quantidadeComprada e preço//
var Fatura = /** @class */ (function () {
    function Fatura(_numeros, _descricao, _quantidadeComprada, _preco) {
        this._numeros = _numeros;
        this._descricao = _descricao;
        this._quantidadeComprada = _quantidadeComprada;
        this._preco = _preco;
    }
    //Crie um método valor da Fatura para multiplicar a quantidade comprada pelo preço.//
    Fatura.prototype.valorDaFatura = function () {
        var valor = this._quantidadeComprada * this._preco;
        return valor;
    };
    return Fatura;
}());
//Criando um objeto computador a partir do modelo Fatura//
var computador = new Fatura(1, 'Computador', 1, 4500);
//Mostrando as informações do objeto criado//
console.log(computador);
//Mostrando o valor da fatura//
console.log("Valor total da Fatura:", computador.valorDaFatura());
